<?php

namespace App\Http\Controllers;

use App\Navigation;
use App\Projects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


class pageController extends Controller
{
    public function index()
    {
        $currentRoute = Route::currentRouteName();
        $navigation = Navigation::all();
        return view('home', ['navigation' => $navigation, 'currentRoute' => $currentRoute]);
    }
    public function showProjects()
    {
        $currentRoute = Route::currentRouteName();
        $projects = Projects::all();
        $navigation = Navigation::all();

        return view('projects',['projects'=> $projects, 'navigation' => $navigation, 'currentRoute' => $currentRoute]);
    }
    public function contact()
    {
        $currentRoute = Route::currentRouteName();
        $navigation = Navigation::all();

        return view('contact',['navigation' => $navigation, 'currentRoute' => $currentRoute]);
    }
}
