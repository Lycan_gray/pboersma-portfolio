<?php

namespace App\Http\Controllers;

use App\Projects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function index(){
        return response()->json(DB::table('navigation_link')->get(), 200);
    }

    public function getProjects(){
        return response()->json(Projects::all()->tech, 200);
    }
    public function getSpecProject(Request $request){
        $id = $request->id;
        $projects = Projects::where('id', $id)->get();
        $allTech = Projects::find($id)->tech;
        return response()->json([$projects, $allTech], 200);

//        return
    }
    public function getProjectsstack(){
        $stack = request('stack');
        return response()->json(Projects::where('section', $stack)->get(), 200);
    }
}
