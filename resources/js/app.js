require('./bootstrap');


import TypeIt from 'typeit';

const instance = new TypeIt('#element', {
    strings : ['PC Enthusiast', 'PC Builder'],
    breakLines: false,
    loop : true,
    speed: 200
}).go();

let projectModal = $('#projectModal');

projectModal.on('show.bs.modal', function (e) {
    axios.get('/api/projects/' + projectId )
        .then(function (response) {
            // handle success
            let project = response.data[0][0];
            let tech = response.data[1];
            let modalProjectImage =  $('#modalProjectImg');

            $('#modalTitle').html(project.project_name);
            $('#modalHeader').css({'background': 'url(' + project.header_img + ') no-repeat center', 'background-size' : 'cover'});
            modalProjectImage.attr('src', project.img_url);
            modalProjectImage.attr('alt', project.project_name);

            $('#modalShortDesc').html(project.short_description);
            $('#modalLongDesc').html(project.long_description);

            tech.forEach(function (element) {
                let techElement = element;
                let techDiv = `
                                   <a href="`+ techElement.tech_link +`" target="_blank" class="mr-2 mb-1 py-1 px-2 rounded techTag">
                                        <i class="fas fa-hashtag mr-1"></i> ` + techElement.tech + `
                                    </a>
                `;
                $('#techStack').append(techDiv);
            });


            let previewButton = $('#previewButton');
            let gitlabButton =   $('#gitLabButton');

            previewButton.show();
            previewButton.attr('href', project.project_link);

            if(project.id === 7){
                previewButton.hide();
            }

            if(project.is_public === 1){
                gitlabButton.show();
                gitlabButton.attr('href', project.git_link);
            }else{
                gitlabButton.hide();
            }
        })
        .catch(function (error) {
        })
        .then(function () {
        });
});

projectModal.on('hidden.bs.modal', function (e) {
    $('#techStack').empty();
});

