@extends('layouts.layout')
@section('page_title', 'Contact')
@section('content')
    <div class="contact container mt-5">
        <h1>Send me a message!</h1>
        <hr class="break-line">
        <form>
            <div class="row">
                <div class="col-6">
                    <div class="form-group px-5">
                        <label style="margin: 1rem -3rem" for="firstName">First name</label>
                        <input type="text" style="margin: 0 -3rem" class="form-control" id="firstName" placeholder="John">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group px-5">
                        <label style="margin: 1rem -3rem" for="firstName">Last name</label>
                        <input type="text" style="margin: 0 -3rem" class="form-control" id="firstName" placeholder="Doe">
                    </div>
                </div>
                <div class="col-12 px-5">
                    <div class="form-group">
                        <label style="margin: 1rem -3rem" for="email">E-Mail Address</label>
                        <input type="text" style="margin: 0 -3rem" class="form-control" id="email" placeholder="johndoe@live.com">
                    </div>
                </div>
                <div class="col-12 px-5">
                    <div class="form-group">
                        <label style="margin: 1rem -3rem" for="message">Example textarea</label>
                        <textarea style="margin: 0 -3rem" class="form-control" id="message" rows="7" placeholder="Message"></textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection