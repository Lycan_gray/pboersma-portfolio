@extends('layouts.layout')
@section('page_title', 'Projects')
@section('content')
    @include('partials/modal')
    <div class="projects container mt-5">
            <h2 class="projects_title mx-3 animated bounceInLeft">Projects</h2>
        <hr class="break-line">
        <div class="row animated fadeIn 3s">
            @foreach($projects as $project)
                <div class="col-12 col-md-6 col-lg-4 p-3">
                    <div class="card_custom hvr-shadow" data-toggle="modal" data-target=".bd-example-modal-xl" onclick="setId({{$project->id}});">
                        <div class="card_background">
                            <img src="{{$project->img_url}}" class="img-fluid">
                        </div>
                        <div class="card_body  hvr-underline-from-center">
                            <div class="card_info p-3">
                                <h3 class="mb-0">{{$project->project_name}}</h3>
                                <p class="mb-0">{{$project->short_description}}</p>
                                    <div class="card_info_buttons mt-2">
                                        @if($project->id != 7)
                                            <a href="{{$project->project_link}}" target="_blank" class="btn btn-outline-primary hvr-icon-pop"><i class="fas fa-external-link-alt mr-1 hvr-icon "></i> Preview</a>
                                        @endif
                                        @if($project->is_public == True)
                                            <a href="{{$project->git_link}}" target="_blank" class="btn btn-outline-primary hvr-icon-pop"><i class="fab fa-gitlab mr-1 hvr-icon "></i> Gitlab</a>
                                        @endif
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
