<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Lycan - @yield('page_title')</title>

    <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/hover.css') }}"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
          integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <script>
        function setId(id){
            projectId = id;
        }
    </script>
</head>
    <body>
            @if($currentRoute != 'home')
                @include('partials/navigation')
            @endif
            @yield('content')
            @if($currentRoute != 'home')
                @include('partials/footer')
            @endif
    <script async src="{{ asset('/js/app.js') }}"></script>
    </body>
</html>
