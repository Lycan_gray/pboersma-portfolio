@extends('layouts.layout')
@section('page_title', 'Home')
@section('content')
    <div class="header p-5 overflow-hidden">
        <div class="container p-1 p-md-5 mt-5">
                <div class="balk ml-1 animated bounceInLeft"></div>
                <h1 class="main_title animated bounceInLeft">Peter Boersma</h1>
                <h3 id="element" class="secondary_title mb-3 animated bounceInLeft">Full-Stack Developer</h3>
            <div class="social animated bounceInLeft">
                <a href="#" class="mr-1 hvr-bounce-in"><i class="fab fa-gitlab"></i></a>
                <a href="#" class="mr-1 hvr-bounce-in"><i class="fab fa-linkedin"></i></a>
                <a href="#" class="mr-1 hvr-bounce-in "><i class="fab fa-twitter"></i></a>
            </div>
            <div class="mt-4 pt-5 ">
                @foreach($navigation as $link)
                        <a href="{{$link->href}}" class="home-buttons mr-1 mb-1 btn btn-outline-primary font-weight-bold">{{$link->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="notice text-center font-italic  ">
            <p class="text-white mb-0">Portfolio made with <a href="https://laravel.com/" class="laravel">Laravel <i class="fab fa-laravel"></i></a> and hosted on  <a href="https://www.digitalocean.com/" class="digitalocean">Digitalocean <i class="fab fa-digital-ocean"></i></a></p>
            <p class="text-white">Copyright &copy; {{date("Y")}} Peter Boersma. All rights reserved.</p>
        </div>
    </div>
@endsection
