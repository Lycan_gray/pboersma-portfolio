<footer class="mt-5">
    <div class="container">
        <div class="row basic_info mt-3">
            <div class="col-1 d-none d-md-block">
                <img src="{{asset('img/me.png')}}" class="img-fluid ">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <p class="mb-1">This website contains all the important information about Peter Boersma. This includes all important projects, Job Experience, Contact information and other important things. <br> Want to know more? <a href="#">Send me an Email!</a></p>
                <p><a href="#">Gitlab</a> / <a href="#">LinkedIn</a> / <a href="#">Twitter</a></p>
            </div>
        </div>
        <div class="row my-3 px-3 text-center text-md-left">
            <div class="col-12 col-md-9 font-italic">
                Portfolio made with <a href="https://laravel.com/" class="laravel">Laravel <i class="fab fa-laravel"></i></a> and hosted on  <a href="https://www.digitalocean.com/" class="digitalocean" >Digitalocean <i class="fab fa-digital-ocean"></i></a><br>
                Copyright &copy; {{date("Y")}} Peter Boersma. All rights reserved.
            </div>
        </div>
    </div>
</footer>
