<div id="projectModal" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div id='modalHeader' class="modal_header mb-lg-5 pt-3 px-3">
                <div class="container modalHeaderContainer">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <img id="modalProjectImg" src="{{asset('img/dc.jpg')}}" class="modalProjectImg img-fluid">
                        </div>
                        <div class="col-9">
                            <div class="modalTitleContainer">
                                <h2 id="modalTitle" class="modalTitle">ProjectName</h2>
                                <div id="techStack" class="tech pt-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container content-container mt-lg-5 p-5">
               <div class="row mb-3">
                   <div class="col-12">
                       <p id="modalShortDesc" class="font-weight-bold">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritati aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ips</p>
                       <p id="modalLongDesc">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
                   </div>
               </div>
                <a id="previewButton" href="#" target="_blank" class="btn btn-outline-primary hvr-icon-pop float-right"><i class="fas fa-external-link-alt mr-1 hvr-icon "></i> Preview</a>
                <a id="gitLabButton" href="#" target="_blank" class="btn btn-outline-primary hvr-icon-pop float-right mr-1"><i class="fab fa-gitlab mr-1 hvr-icon "></i> Gitlab</a>
            </div>

        </div>
    </div>
</div>